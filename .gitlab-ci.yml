stages:
  - build
  - test
  - publish

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/cache/pip"

before_script:
  - python -V
  - python -m venv cache/venv
  - source cache/venv/bin/activate
  - pip install --upgrade pip
  - pip install -r requirements/ci.txt

.py37:
  image: python:3.7
  cache:
    key: py37
    paths:
      - cache

.py38:
  image: python:3.8
  cache:
    key: py38
    paths:
      - cache

.py39:
  image: python:3.9
  cache:
    key: py39
    paths:
      - cache

build:
  stage: build
  extends: .py39
  script:
    - python -m build
  artifacts:
    paths:
      - dist/*

.codetest:
  stage: test
  extends: .py39
  needs: []

test:format:
  extends: .codetest
  script:
    - nox -s format

test:imports:
  extends: .codetest
  script:
    - nox -s imports

test:lint:
  extends: .codetest
  script:
    - nox -s lint

test:manifest:
  extends: .codetest
  script:
    - nox -s manifest

test:doctest:
  extends: .codetest
  script:
    - nox -s doctest

test:coverage:
  extends: .codetest
  script:
    - nox -s coverage

.typing:
  stage: test
  needs: ["build"]
  script:
    - nox -s typing

.unit:
  stage: test
  needs: ["build"]
  script:
    - nox -s unittest
  artifacts:
    reports:
      junit: tests_py3*.xml

test:typing-py37:
  extends: [".py37", ".typing"]

test:unit-py37:
  extends: [".py37", ".unit"]

test:typing-py38:
  extends: [".py38", ".typing"]

test:unit-py38:
  extends: [".py38", ".unit"]

test:typing-py39:
  extends: [".py39", ".typing"]

test:unit-py39:
  extends: [".py39", ".unit"]

test:dists:
  stage: test
  extends: .py39
  needs: ["build"]
  script:
    - twine check --strict dist/*

.publish:
  stage: publish
  extends: .py39
  script:
    - twine upload dist/*

publish:test.pypi.org:
  extends: .publish
  variables:
    TWINE_REPOSITORY_URL: https://test.pypi.org/legacy/
  environment:
    name: test.pypi.org
    url: https://test.pypi.org/project/pupygrib/
  only:
    - /^\d+\.\d+\.\d+rc\d+$/
  except:
    - branches

publish:pypi.org:
  extends: .publish
  environment:
    name: pypi.org
    url: https://pypi.org/project/pupygrib/
  only:
    - /^\d+\.\d+\.\d+$/
  except:
    - branches
